<?php

namespace App\Helpers;

class S3
{
    private static function getBaseUrl()
    {
    	return config('storage.path');
    }

    public static function mountUrl($fileWithPath) 
    {
        return sprintf('%s%s', self::getBaseUrl(), $fileWithPath);
    }
}