<?php

namespace App\Helpers;

trait ResponsesTrait 
{
    /**
     * Respond with status 200 OK
     * 
     * @param type $responseContents 
     * @return json
     */
    public function respondWithOk($responseContents)
    {
        return $this->respond($responseContents, 200, []);
    }

    /**
     * Respond with status 400 not found
     * 
     * @param string $error 
     * @return json
     */
    public function respondWithBadRequest($error='The server cannot process this request due to apparent client error')
    {
        return $this->respondWithError($error, 400, []);
    }

    /**
     * Respond with status 401 unauthorized
     * 
     * @param string $error 
     * @return json
     */
    public function respondWithUnauthorized($error='You are not authorized to access this resource')
    {
        return $this->respondWithError($error, 401, []);
    }

    /**
     * Respond with status 404 not found
     * 
     * @param string $error 
     * @return json
     */
    public function respondWithNotFound($error='Resource not found!')
    {
        return $this->respondWithError($error, 404, []);
    }

    /**
     * Respond with status 406 not acceptable
     * 
     * @param string $error 
     * @return json
     */
    public function respondWithNotAcceptable($error)
    {
        return $this->respondWithError($error, 406, []);
    }
    
    /**
     * Respond with status 500 acceptable
     * 
     * @param string $error 
     * @return json
     */
    public function respondWithInternalError($error='We encountered a non-identified error, please try again later')
    {
        return $this->respondWithError($error, 500, []);
    }
    
    /**
     * Return response
     * 
     * @param string $responseContents 
     * @param integer $statusCode 
     * @param array $headers 
     * @return json
     */
    private function respond($responseContents, $statusCode, $headers=[]) 
    {
        return response()->json($responseContents, $statusCode, $headers);
    }

    /**
     * Return error response
     * 
     * @param string $error 
     * @param integer $statusCode 
     * @param array $headers 
     * @return json
     */
    private function respondWithError($error, $statusCode, $headers=[]) 
    {
        return response()->json(['error' => ['message' => $error]], $statusCode, $headers);
    }
}