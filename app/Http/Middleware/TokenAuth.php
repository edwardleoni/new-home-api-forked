<?php

namespace App\Http\Middleware;

use App\Helpers\ResponsesTrait;
use App\Repositories\Internals\Contracts\TokenRepositoryInterface;
use Closure;

class TokenAuth
{
    use ResponsesTrait;
    /**
     * @var \App\Repositories\Internals\Contracts\TokenRepositoryInterface
     */
    protected $tokenRepository;

    /**
     * @param TokenRepositoryInterface $tokenRepository 
     * @return type
     */
    public function __construct(TokenRepositoryInterface $tokenRepository) 
    {
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->input('token');

        if ($token === null) {
            return $this->respondWithUnauthorized('Token not provided');
        }

        if (!$this->tokenRepository->isValid($token)) {
            return $this->respondWithUnauthorized('Invalid or expired token provided');
        }

        return $next($request);
    }
}
