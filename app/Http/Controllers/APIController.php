<?php

namespace App\Http\Controllers;

use App\Helpers\ResponsesTrait;
use Laravel\Lumen\Routing\Controller as BaseController;

class APIController extends BaseController
{
    use ResponsesTrait;

    public function __construct() {}

    /**
     * Transform a collection
     * 
     * @param Collection $item 
     * @return array
     */
    public function transformCollection($item) 
    {
        if (!is_array($item)) {
            $item = $item->toArray();
        }

        return array_map([$this, 'transform'], $item);
    }

     /**
     * Generate pagination
     * 
     * @param paginateableObject $paginateableObject 
     * @return array
     */
    public function makePaginator($paginateableObject)
    {
        return [
            'total_count' => $paginateableObject->total(),
            'current_page' => $paginateableObject->currentPage(),
            'total_pages' => $paginateableObject->lastPage(),
            'limit' => count($paginateableObject)
        ];
    }
}