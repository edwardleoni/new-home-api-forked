<?php

namespace App\Http\Controllers;

use App\Helpers\S3;
use App\Http\Controllers\APIController;
use App\Repositories\Externals\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class BuilderController extends APIController
{
    /**
     * @var 
     */
    private $listingRepository; 
    
    /**
     * @var 
     */
    private $userRepository;
    
    /**
     * @param UserRepositoryInterface $userRepository 
     */
    public function __construct(UserRepositoryInterface $userRepository) 
    {
        parent::__construct();
        
        $this->userRepository = $userRepository;
    }

    /**
     * List items
     * 
     * @param string|null $listingId 
     * @return json
     */
    public function list(Request $params, $builderId=null)
    {
        if ($builderId === null) {

            /**
             * This try catch is a horrible solution, as it assumes there's no errors on our end
             * this needs changing 
             */
            try {
                $builders = $this->userRepository->searchAndFilterBuilders($params->all());
            } catch (\Exception $e) {
                $handler = new ExceptionHandler;
                $handler->report($e);

                return $this->respondWithBadRequest();
            }
            
            $builders = $builders->paginate($this->userRepository::RESULTS_LIMIT);

            $data = [
                'data' => $this->transformCollection($builders->all()),
                'paginator' => $this->makePaginator($builders)
            ];

            return $this->respondWithOk($data);

        } else {

            $builder = $this->userRepository->getById($builderId);
            
            if (!$builder) {
                return $this->respondWithNotFound();
            }
            
            return $this->transform($builder);

        }
        
    }

    /**
     * Transform a single builder
     * 
     * @param  Listing $listing 
     * @return json
     */
    public function transform($listing)
    {
        // When not short in time, redo this as it's simply transmitting everything from the database, do it like ListingController@transformGeneric
        
        $listing["id"] = $listing["_id"];
        unset($listing["_id"]);
        $listing["validated"] = (bool) $listing["validated"];
        $listing["logo"] = S3::mountUrl($listing["logo"]);
        $listing["brochure"] = S3::mountUrl($listing["brochure"]);

        return $listing;
    }

}