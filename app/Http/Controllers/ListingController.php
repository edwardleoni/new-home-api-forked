<?php

namespace App\Http\Controllers;

use App\Helpers\S3;
use App\Http\Controllers\APIController;
use App\Repositories\Externals\Contracts\ListingRepositoryInterface;
use App\Repositories\Externals\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class ListingController extends APIController
{
    /**
     * @var 
     */
    private $listingRepository; 
    
    /**
     * @var 
     */
    private $userRepository;
    
    /**
     * @param ListingRepositoryInterface $listingRepository 
     * @param UserRepositoryInterface $userRepository 
     */
    public function __construct(ListingRepositoryInterface $listingRepository, UserRepositoryInterface $userRepository) 
    {
        parent::__construct();
        
        $this->listingRepository = $listingRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * List items
     * 
     * @param string|null $listingId 
     * @return json
     */
    public function list(Request $params, $listingId=null)
    {
        if ($listingId === null) {

            // This try catch is a horrible solution, as it assumes there's no errors on our end
            // this needs changing
            try {
                $listings = $this->listingRepository->searchAndFilter($params->all());
            } catch (\Exception $e) {
                $handler = new ExceptionHandler;
                $handler->report($e);

                return $this->respondWithBadRequest();
            }
            
            $listings = $listings->paginate($this->listingRepository::RESULTS_LIMIT);

            $data = [
                'data' => $this->transformCollection($listings->all()),
                'paginator' => $this->makePaginator($listings)
            ];

            return $this->respondWithOk($data);

        } else {

            $listing = $this->listingRepository->getById($listingId);
            
            if (!$listing) {
                return $this->respondWithNotFound();
            }
            
            return $this->transform($listing);

        }
        
    }

    /**
     * Transform a single listing
     * 
     * @param  Listing $listing 
     * @return json
     */
    public function transform($listing)
    {
        switch ($listing['type']) {
            case 'house-plan':
                return $this->transformHousePlan($listing);
                break;
            case 'show-home':
                return $this->transformShowHome($listing);
                break;
            case 'house-and-land':
                return $this->transformHouseAndLand($listing);
                break;
            case 'development':
                return $this->transformDevelopment($listing);
                break;
            default: 
        }
              
    }

    /**
     * Transform house plan
     * 
     * @param  Listing $listing 
     * @return json
     */
    private function transformHousePlan($listing)
    {
        $transformation = $this->transformGeneric($listing);

        $transformation['price'] = (float) $listing['price'];

        return $transformation;
    }

    /**
     * Transform house plan
     * 
     * @param  Listing $listing 
     * @return json
     */
    private function transformShowHome($listing)
    {
        $transformation = $this->transformGeneric($listing);

        $transformation['location'] = $listing['location'];

        return $transformation;
    }

    /**
     * Transform house plan
     * 
     * @param  Listing $listing 
     * @return json
     */
    private function transformHouseAndLand($listing)
    {
        $transformation = $this->transformGeneric($listing);

        $transformation['price'] = (float) $listing['price'];
        $transformation['location'] = $listing['location'];

        return $transformation;
    }

    /**
     * Transform house plan
     * 
     * @param  Listing $listing 
     * @return json
     */
    private function transformGeneric($listing)
    {
        return [
            'id'  =>  $listing['_id'],
            'type' => $listing['type'], 
            'title' => $listing['title'], 
            'type' => $listing['type'],  
            'price' => (float) $listing['price'],  
            'location' => $listing['build_locations'], 
            'description' => $listing['description'], 
            'bedrooms' => (int) $listing['bedrooms'], 
            'bathrooms' => (int) $listing['bathrooms'], 
            'floor_area' => (int) $listing['floor_area'], 
            'parking_spaces' => (int) $listing['parking_spaces'], 
            'alfresco' => (bool) $listing['alfresco'], 
            'storeys' => (int) $listing['storeys'], 
            'living_areas' => (int) $listing['living_areas'], 
            'block_length' => (int) $listing['block_length'], 
            'block_width' => (int) $listing['block_width'], 
            'dual_occupancy' => (bool) $listing['dual_occupancy'], 
            'inclusions' => $listing['inclusions'], 
            'gallery' => $this->prepareGalleryForTransformation($listing['gallery']),
            'builder' => [
                'id'  => $listing['user']['_id'],
                'name'  => $listing['user']['name'],
                'logo'  => S3::mountUrl($listing['user']['logo']),
            ]
        ];
    }

    /**
     * Add the AmazonS3 prefix to the path of the images in the gallery
     * 
     * @param  Array $gallery 
     * @return json
     */
    private function prepareGalleryForTransformation($gallery)
    {
        for ($i=0; $i < count($gallery); $i++) {
            $gallery[$i] = S3::mountUrl($gallery[$i]);
        }

        return $gallery;
    }
}