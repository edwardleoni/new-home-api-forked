<?php

namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Repositories\Internals\Contracts\TokenRepositoryInterface;
use App\Repositories\Internals\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;

class AuthController extends APIController
{
    /**
     * @var
     */
    private $tokenRepository;

    /**
     * @var
     */
    private $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository 
     * @param TokenRepositoryInterface $tokenRepository 
     */
    public function __construct(UserRepositoryInterface $userRepository, TokenRepositoryInterface $tokenRepository) 
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * Authenticate a user
     * @param Request $request 
     * @return Array
     */
    public function auth(Request $request)
    {
        $user = $this->userRepository->validate($request->all());

        if (isset($user['error'])) {
            return $this->respondWithNotAcceptable($user['error']);
        }

        $token = $this->tokenRepository->generateAndSet($user->id);

        return $this->respondWithOk(['token' => $token], false);
    }

    /**
     * Test authentication token
     * @param Request $request 
     * @return Array
     */
    public function test(Request $request)
    {
        $response = $this->tokenRepository->isValid($request->input('token'));

        If ($response === true) {
            return $this->respondWithOk(['success' => 'You are using a valid token'], false);
        }

        return $this->respondWithUnauthorized('Your token is invalid');
    }
}