<?php

namespace App\Jobs;

use App\Repositories\Internals\Contracts\TokenRepositoryInterface;

/**
 * LEGACY CODE, boiler plate left in here just in case
 */
class RemoveExpiredToken extends Job
{
    private $index;

    private $tokenRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tokenRepository)
    {
        $this->tokenRepository = $tokenRepository ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->tokenRepository->removeExpiredToken();
    }
}