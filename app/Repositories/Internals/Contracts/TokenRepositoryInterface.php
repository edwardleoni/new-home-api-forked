<?php

namespace App\Repositories\Internals\Contracts;

interface TokenRepositoryInterface
{
    CONST TOKEN_EXPIRATION = 15;

    CONST CACHE_EXPIRATION = 2;
    
    public function isValid($token);

    public function generateAndSet($userId);

    public function generate($userId);
    
    public function set($token, $userId);
}