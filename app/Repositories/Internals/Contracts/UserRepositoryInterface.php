<?php

namespace App\Repositories\Internals\Contracts;

interface UserRepositoryInterface
{
    /**
     * Specify how long the cache should last for
     */
    const CACHE_EXPIRATION = 2;

    public function validate(array $credentials);
}