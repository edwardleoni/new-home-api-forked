<?php

namespace App\Repositories\Internals;

use App\Jobs\RemoveExpiredToken;
use App\Models\Internals\Token;
use App\Repositories\Internals\Contracts\TokenRepositoryInterface;
use App\Repositories\GenericRepository;
use Carbon\Carbon;
use Cache;

class TokenRepository extends GenericRepository implements TokenRepositoryInterface
{
    /**
     * @param Token $token 
     */
    public function __construct(Token $token)
    {
        $this->token = $token;
    }
    
    /**
     * Check if a token is valid
     * 
     * @param string $token 
     * @return bool
     */
    public function isValid($token)
    {
        $search = Cache::remember("token_$token", self::CACHE_EXPIRATION, function () use ($token) {
            return $this->token->where('token', $token)
                              ->where('created_at', '>=', Carbon::now()->subminutes(self::TOKEN_EXPIRATION))
                              ->count();
        });

        if ($search > 0) {
            return true;
        }

        return false;
    }
    
    /**
     * Generate a token and set it
     * 
     * @param string $userId 
     * @return string
     */
    public function generateAndSet($userId)
    {
        $token = $this->generate($userId);

        return $this->set($token, $userId);
    }

    /**
     * Generate a new token
     * 
     * @return string
     */
    public function generate($userId)
    {
        $random = rand(0, 17391073127301737);
        $time = time();
        $md5 = md5($random . $time);

        return base64_encode($md5) . '_' . $userId;
    }
    
    /**
     * Set a token
     *      
     * @param string $token 
     * @param string $userId 
     * @return string
     */
    public function set($token, $userId)
    {
        $this->token->create([
            'token' => $token,
            'user_id' => $userId
        ]);

        return $token;
    }  
}