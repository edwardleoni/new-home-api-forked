<?php

namespace App\Repositories\Internals;

use App\Repositories\Internals\Contracts\UserRepositoryInterface;
use App\Repositories\GenericRepository;
use App\Models\Internals\User;

class UserRepository extends GenericRepository implements UserRepositoryInterface
{
    /**
     * @param User $model 
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }   

    /**
     * Checks user login information
     * @param array $credentials 
     * @return mixed
     */
    public function validate(array $credentials) 
    {
        if (!isset($credentials['username'])) {
            return ['error' => 'Username not provided'];
        }

        if (!isset($credentials['password'])) {
            return ['error' => 'Password not provided'];
        }
        
        // Search for user with the password in the database
        $search = $this->model->where('username', $credentials['username'])
                              ->where('password', md5($credentials['password']))
                              ->first();

        if (count($search) === 0) {
            return ['error' => 'Invalid credentials provided'];
        }

        return $search;
    }
}