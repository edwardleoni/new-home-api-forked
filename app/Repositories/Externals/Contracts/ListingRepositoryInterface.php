<?php

namespace App\Repositories\Externals\Contracts;


interface ListingRepositoryInterface
{
    /**
     * Specify how long the cache should last for
     */
    const CACHE_EXPIRATION = 2;

    /**
     * Specify how many results should appear per page
     */
    const RESULTS_LIMIT = 10;

    public function getById($listingId);

    public function searchAndFilter($params);
}