<?php

namespace App\Repositories\Externals\Contracts;

interface UserRepositoryInterface
{
    /**
     * Specify how many results should appear per page
     */
    const RESULTS_LIMIT = 10;

    public function getById($userId);

    public function searchAndFilterBuilders($params);
}