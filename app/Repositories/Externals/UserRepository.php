<?php

namespace App\Repositories\Externals;

use App\Helpers\ResponsesTrait;
use App\Repositories\Externals\Contracts\UserRepositoryInterface;
use App\Repositories\GenericRepository;
use App\Models\Externals\Role;
use App\Models\Externals\User;

class UserRepository extends GenericRepository implements UserRepositoryInterface
{
    use ResponsesTrait;

    /**
     * @param Listing $model 
     */
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * Get a user by its id
     * 
     * @param string $userId 
     * @return User
     */
    public function getById($userId) 
    {
        // Uncoment this when solution is found for Serialization of 'MongoDB\BSON\ObjectID' is not allowed
        //return Cache::remember('user_{$userId}', self::CACHE_LENGTH, function () use ($userId) {
            return $this->user->find($userId);
        //});
    }

    /**
     * Search for builders and filters them
     * 
     * @param Request $params 
     * @return User Collection
     */
    public function searchAndFilterBuilders($params)
    {
        $builderRoleId = $this->getRoleIdByName('Builder');
        
        $results = $this->user;
        $results = $results->where('role_id', $builderRoleId);

        return $results;
    }

    /**
     * Get a role ID by name
     * 
     * @param String $name 
     * @return Role
     */
    private function getRoleIdByName($name)
    {
        return $this->getRoleByName($name)->_id;
    }

    /**
     * Search for a role by name
     * 
     * @param String $name 
     * @return Role
     */
    private function getRoleByName($name)
    {
        return $this->role->where('name', $name)->first();
    }
}