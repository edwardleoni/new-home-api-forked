<?php

namespace App\Repositories\Externals;

use App\Helpers\ResponsesTrait;
use App\Models\Externals\Listing;
use App\Repositories\Externals\Contracts\ListingRepositoryInterface;
use App\Repositories\GenericRepository;
use Cache;

class ListingRepository extends GenericRepository implements ListingRepositoryInterface
{
    use ResponsesTrait;

    /**
     * @param Listing $model 
     */
    public function __construct(Listing $listing)
    {
        $this->listing = $listing;
    }

    /**
     * Get a listing by its id
     * 
     * @param string $listingId 
     * @return Listing
     */
    public function getById($listingId) 
    {
        // Uncoment this when solution is found for Serialization of 'MongoDB\BSON\ObjectID' is not allowed
        //return Cache::remember('listing_{$listingId}', self::CACHE_LENGTH, function () use ($listingId) {
            return $this->listing->find($listingId);
        //});
    }

    /**
     * Search for listings and filters them
     * 
     * @param Request $params 
     * @return Listing Collection
     */
    public function searchAndFilter($params)
    {
        $results = $this->listing;

        $results = $this->includeBuilderMeta($results);

        $results = $this->filterType($results, $params);
        $results = $this->filterBuildRegion($results, $params); // To code
        $results = $this->filterBedrooms($results, $params);
        $results = $this->filterBathrooms($results, $params);
        $results = $this->filterParkingSpaces($results, $params);
        $results = $this->filterLivingAreas($results, $params);
        $results = $this->filterStoreys($results, $params);
        $results = $this->filterAlfresco($results, $params);
        $results = $this->filterDualOccupancy($results, $params);
        $results = $this->filterShowHomes($results, $params); // To code
        $results = $this->filterSpecificBuilder($results, $params); // To code
        $results = $this->filterInclusions($results, $params); // To code
        $results = $this->filterPrice($results, $params);
        $results = $this->filterFloorArea($results, $params);
        $results = $this->filterBlockLength($results, $params);
        $results = $this->filterBlockWidth($results, $params);
        $results = $this->filterBuilder($results, $params);

        $results = $this->order($results, $params);

        return $results;
    }

    /**
     * Order the result
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    public function order($results, $params)
    {
        if (!$this->verifyIfFilteringIsNeeded('order', $params)) {
            return $results;
        }

        // Separate order field from order direction
        $order = explode(",", $params['order']);

        // Direction may be empty
        if (!isset($order[1])) {
            $order[1] = 'asc';
        }

        return $results->orderBy($order[0], $order[1]);
    }

    /**
     * Include builder metadata
     *
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function includeBuilderMeta($results) 
    {
        return $results->with('user');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterBuildRegion($results, $params)
    {
        if (!$this->verifyIfFilteringIsNeeded('build_region', $params)) {
            return $results;
        }

        return $results; // logic needed here
        // Apply haversine formula here to find locations around a 50k area
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterBedrooms($results, $params)
    {
        return $this->multipleComparisonFilter($results, $params, 'bedrooms');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterBathrooms($results, $params)
    {
        return $this->multipleComparisonFilter($results, $params, 'bathrooms');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterParkingSpaces($results, $params)
    {
        return $this->multipleComparisonFilter($results, $params, 'parking_spaces');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterLivingAreas($results, $params)
    {
        return $this->multipleComparisonFilter($results, $params, 'living_areas');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterStoreys($results, $params)
    {        
        return $this->multipleComparisonFilter($results, $params, 'storeys');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterAlfresco($results, $params)
    {
        return $this->booleanFilter($results, $params, 'alfresco');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterType($results, $params)
    {
        return $this->equalsToFilter($results, $params, 'type');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterBuilder($results, $params)
    {
        return $this->equalsToFilter($results, $params, 'builder_id');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterDualOccupancy($results, $params)
    {
        return $this->booleanFilter($results, $params, 'dual_occupancy');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterShowHomes($results, $params)
    {
        /**
        Display home is another word for show home, lets just use the word showhome to avoid confusion. It's basically sorting properties that are have an existing association with a showhome listing. Ie if you like the property there's a corresponding showhome listing you can visit.
        *\/*/
        return $results; // logic needed here
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterSpecificBuilder($results, $params)
    {
        return $this->equalsToFilter($results, $params, 'builder_id');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterInclusions($results, $params)
    {
        if (!$this->verifyIfFilteringIsNeeded('inclusions', $params)) {
            return $results;
        }

        $inclusions = explode($params['inclusions'], ',');
        foreach ($inclusions  as $inclusion) {
            $query = $query->where('inclusions', 'LIKE', '%{$inclusion}%');
        }

        return $query;
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterPrice($results, $params)
    {
        return $this->rangeFilter($results, $params, 'price');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterFloorArea($results, $params)
    {
        return $this->rangeFilter($results, $params, 'floor_area');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterBlockLength($results, $params)
    {
        return $this->rangeFilter($results, $params, 'block_length');
    }

    /**
     * Apply filter
     * 
     * @param Request $results 
     * @param Request $params 
     * @return Listing Collection
     */
    private function filterBlockWidth($results, $params)
    {
        return $this->rangeFilter($results, $params, 'block_width');
    }

    /**
     * Apply a filter that recognises >=, <=, !=, and = as comparison method
     * 
     * @param String $filter 
     * @param Request $params 
     * @param String $field
     * @return Moloquent
     */
    private function multipleComparisonFilter($results, $params, $field)
    {
        if (!$this->verifyIfFilteringIsNeeded($field, $params)) {
            return $results;
        }

        $comparisonMethod = $this->findComparisonMethod($params[$field]);
        $compareTo = (int) $this->cleanStringFromComparisonMethods($params[$field]);

        if ($comparisonMethod === "=") {
            return $this->equalsToFilter($results, $params, $field);
        }

        return $results->where($field, $comparisonMethod, $compareTo);
    }

    /**
     * Apply a filter of the type range
     * e.g: Greater than or equal to something and less than or equal to something else
     * 
     * @param String $filter 
     * @param Request $params 
     * @param String $field
     * @return Moloquent
     */
    private function rangeFilter($results, $params, $field)
    {
        if (!$this->verifyIfFilteringIsNeeded($field, $params)) {
            return $results;
        }

        $range = explode(',', $params[$field]);

        return $results->where($field, '>=', (int) $range[0])->where($field, '<=', (int) $range[1]);
    }
    
    /**
     * Apply a filter of the equal to
     * e.g: Param for field equals to what's in the database
     * 
     * @param String $filter 
     * @param Request $params 
     * @param String $field
     * @return Moloquent
     */
    private function equalsToFilter($results, $params, $field)
    {
        if (!$this->verifyIfFilteringIsNeeded($field, $params)) {
            return $results;
        }

        // An attempt to keep consistency
        if ($field === 'builder_id') {
            return $results->where('user_id', $params[$field]);  
        }

        return $results->where($field, $params[$field]);    
    }

    /**
     * Apply a boolean filter
     * Boolean values are actually strings, API expects ("true" or "false")
     * 
     * @param String $filter 
     * @param Request $params 
     * @param String $field
     * @return Moloquent
     */
    private function booleanFilter($results, $params, $field)
    {
        if (!$this->verifyIfFilteringIsNeeded($field, $params)) {
            return $results;
        }
    
        if ($params[$field] === "true") {
            return $results->where($field, true);
        } 

        if ($params[$field] === "false") {
            return $results->where($field, false);
        }

        return $results;
    }

    /**
     * Check if a filter needs applying
     * 
     * @param String $field 
     * @param Request $params 
     * @return bool
     */
    private function verifyIfFilteringIsNeeded($field, $params)
    {
        if ((isset($params[$field])) && (!empty($this->cleanStringFromComparisonMethods($params[$field])))) {
            return true;
        }

        return false;
    }

    /**
     * Some filters contain their comparison methods passed as string
     * This method gets them and isolate from the rest of the string, 
     * then gives it back to mount the query
     * 
     * @param String $string 
     * @return bool
     */
    private function findComparisonMethod($string)
    {
        if (strpos($string, '>=')) {
            return '>=';
        }

        if (strpos($string, '<=')) {
            return '<=';
        }

        if (strpos($string, '!=')) {
            return '!=';
        }

        if (strpos($string, '>')) {
            return '>';
        }

        if (strpos($string, '>')) {
            return '<';
        }

        return '=';
    }

    /**
     * As opposite to findComparisonMethod, this method removes the comparison
     * characters from the string and gives a clean string back to mount the query
     * 
     * @param String $string 
     * @return bool
     */
    private function cleanStringFromComparisonMethods($string)
    {
        $string = str_replace('>', '', $string);
        $string = str_replace('<', '', $string);
        $string = str_replace('=', '', $string);
        $string = str_replace('!', '', $string);
        $string = str_replace('"', '', $string);
        $string = str_replace("'", '', $string);

        return $string;
    }
}