<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
        	'App\Repositories\Internals\Contracts\UserRepositoryInterface',
        	'App\Repositories\Internals\UserRepository'
        );

        $this->app->bind(
        	'App\Repositories\Internals\Contracts\TokenRepositoryInterface',
        	'App\Repositories\Internals\TokenRepository'
        );

        $this->app->bind(
        	'App\Repositories\Externals\Contracts\UserRepositoryInterface',
        	'App\Repositories\Externals\UserRepository'
        );

        $this->app->bind(
        	'App\Repositories\Externals\Contracts\ListingRepositoryInterface',
        	'App\Repositories\Externals\ListingRepository'
        );
        
        // Add Storage to the configuration
        $this->mergeConfigFrom(
            __DIR__.'/../../config/storage.php', 'storage'
        );
    }
}
