<?php

namespace App\Models\Internals;

use Illuminate\Database\Eloquent\Model;

class Token extends Model 
{
	 /**
     * Set the connection for this model
     */
    protected $connection = 'internal';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'token',
    ];
}