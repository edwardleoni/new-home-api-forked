<?php

namespace App\Models\Internals;

use Illuminate\Database\Eloquent\Model; 

class User extends Model
{
    /**
     * Set the connection for this model
     */
    protected $connection = 'internal';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
