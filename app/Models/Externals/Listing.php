<?php

namespace App\Models\Externals;

use Moloquent\Eloquent\Model as Moloquent;
use DB;

class Listing extends Moloquent
{
    /**
     * Set the connection for this model
     */
    protected $connection = 'external';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'category', 'price', 'area', 'bedrooms', 'bathrooms', 'description', 'amenities', 'localisation', 'lat', 'lng', 'gallery', 'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * Quickly get the user who created the listing
     *
     * @return App\Model\Role
     */
    public function user()
    {
        return $this->belongsTo('\App\Models\Externals\User', 'user_id', '_id');
    }
}
