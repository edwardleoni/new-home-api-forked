<?php

namespace App\Models\Externals;

use Moloquent\Eloquent\Model as Moloquent;
use DB;

class User extends Moloquent
{
    /**
     * Set the connection for this model
     */
    protected $connection = 'external';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'location', 'areas_of_service', 'brochure', 'logo', 'description', 'facebook', 'gplus', 'twitter', 'skype', 'website', 'annual_bids', 'role_id', 'activated',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at',
    ];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * Connect a user to his listings
     *
     */
    public function listings()
    {
        return $this->hasMany('\App\Models\Externals\Listing', 'user_id', '_id');
    }

}
