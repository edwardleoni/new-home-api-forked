<?php

namespace App\Models\Externals;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Moloquent\Eloquent\Model as Moloquent;

class Role extends Moloquent
{
    /**
     * Set the connection for this model
     */
    protected $connection = 'external';
    
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];
    
    public function users()
    {
        return $this->hasMany('\App\Models\Externals\Role', 'role_id', '_id');
    }
}
