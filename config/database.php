<?php

return [
    
    'default' => env('DB_CONNECTION', 'internal'),

    'migrations' => 'migrations',
    
    'connections' => [

        'internal' => [
            'driver' => 'mysql',
            'host'   => env('MYSQL_HOST', 'localhost'),
            'port'   => env('MYSQL_PORT', '3306'),
            'database'  => env('MYSQL_DATABASE', 'forge'),
            'username'  => env('MYSQL_USERNAME', 'forge'),
            'password'  => env('MYSQL_PASSWORD', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ], 

        'external' => [
            'driver'   => 'mongodb',
            'host'     => env('MONGO_HOST'),
            'port'     => env('MONGO_PORT', 27017),
            'database' => env('MONGO_DATABASE'),
            'username' => env('MONGO_USERNAME'),
            'password' => env('MONGO_PASSWORD'),
            'options'  => ['socketTimeoutMS' => '1200000']
        ],


    ],

    'redis' => [
        'cluster' => false,

        'default' => [
            'host' => env('REDIS_HOST', ''),
            'pass' => env('REDIS_PASSWORD', null),
            'database' => 0,
        ],
    ],

];