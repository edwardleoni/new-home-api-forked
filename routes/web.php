<?php

$app->group(['prefix' => 'v1'], function () use ($app) {

	$app->post('auth', 'AuthController@auth');
	$app->get('auth/test', 'AuthController@test');
    
    $app->get('listings', ['middleware' => ['tokenAuth'], 'uses' => 'ListingController@list']);
    $app->get('listings/{listingId}', ['middleware' => ['tokenAuth'], 'uses' => 'ListingController@list']);

    $app->get('builders', ['middleware' => ['tokenAuth'], 'uses' => 'BuilderController@list']);
    $app->get('builders/{builderId}', ['middleware' => ['tokenAuth'], 'uses' => 'BuilderController@list']);

});