This is a [Lumen](http://lumen.laravel.com) application that reads from a [MongoDB](http://mongodb.com) database which contains information stored by the [User Panel](https://bitbucket.org/newhomeconz/panel), a [MariaDB](https://mariadb.org/) database where the users allowed to retrieve API information and its tokens are stored. The App uses [Redis](http://redis.io) for caching.

This is a [Laravel](http://laravel.com) application that stores data to a [MongoDB](http://mongodb.com) database.

## Useful commands ##

**Install all PHP dependencies:**
```
#!bash

composer install
```

**Create MySQL/MariaDB database structure for storing users and tokens**
```
#!bash

php artisan migrate
```